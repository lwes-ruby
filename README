= LWES - Light Weight Event System bindings for Ruby

* http://www.lwes.org
* git://reality-escapes.me/git/lwes-ruby.git

== DESCRIPTION:

The LWES Light-Weight Event System is a framework for allowing the exchange of
information from many machines to many machines in a controlled, platform
neutral, language neutral way.  The exchange of information is done in a
connectless fashion using multicast or unicast UDP, and using self describing
data so that any platform or language can translate it to it's local dialect.

Instead of blindly using SWIG-generated bindings and exposing users to the
underlying C APIs, we've wrapped the underlying C library bindings in an
easy-to-use Ruby library.  Currently we only support emitting events but
may support listening and journaling capabilities as time allows.

== FEATURES:

* easy-to-use, "Ruby-ish" API, no manual memory management
* optional ESF (event specification format) validation support

== SUPPORT:

Email the author: Erik S. Chang mailto:esc@reality-escapes.me and expect a
response within 72 hours.

== DEVELOPMENT:

Our git repository is here:

  git clone git://reality-escapes.me/git/lwes-ruby.git

Email pull requests or patches to the the author
mailto:esc@reality-escapes.me

== INSTALL:

This library is easy to install, if you have the LWES library already
installed you can use that, otherwise the RubyGems installer will
automatically download and install a private copy only for use with your
gem.

  gem install lwes

== SYNOPSIS

See link:LWES.html

== NON-ESF USERS:

For prototyping and development, it may be easier to not use an ESF
file.  In those cases, you may skip the TypeDB steps entirely and
just use an emitter to send Hash objects.

See "NON-ESF USERS" section in link:LWES/Emitter.html
